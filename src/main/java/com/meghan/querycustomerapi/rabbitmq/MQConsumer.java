package com.meghan.querycustomerapi.rabbitmq;

import com.meghan.querycustomerapi.axon.command.PlaceOrderCommand;
import com.meghan.querycustomerapi.axon.event.OrderPlacedEvent;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class MQConsumer {

    @Autowired
    private CommandGateway commandGateway;

    @RabbitListener(queues = "${customer.rabbitmq.queue}", containerFactory = "jsaFactory")
    public void recievedMessage(OrderPlacedEvent event) {

        System.out.println("Recieved Message From RabbitMQ: " + event);
        commandGateway.send(new PlaceOrderCommand(event.getId(), event.getMerchantId(), event.getMenuItems()));
    }
}
