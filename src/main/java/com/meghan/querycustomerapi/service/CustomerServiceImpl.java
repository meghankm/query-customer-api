package com.meghan.querycustomerapi.service;

import com.meghan.querycustomerapi.axon.event.OrderPlacedEvent;
import com.meghan.querycustomerapi.entity.CustomerOrder;
import com.meghan.querycustomerapi.entity.repository.CustomerOrderRepository;
import org.axonframework.queryhandling.QueryGateway;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.concurrent.ExecutionException;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private QueryGateway queryGateway;

    @Autowired
    private CustomerOrderRepository customerOrderRepository;

    @Override
    public CustomerOrder getOrder(String customerId) throws ExecutionException, InterruptedException {
        return queryGateway.query(new OrderPlacedEvent(customerId, null, null, null),
                CustomerOrder.class).get();
    }

    @QueryHandler
    public CustomerOrder getCustomerOrderHandler(OrderPlacedEvent query) throws InterruptedException, ExecutionException {
        Optional<CustomerOrder> optional = customerOrderRepository.findById(query.getId());
        return optional.isPresent() ? optional.get() : null;
    }
}
