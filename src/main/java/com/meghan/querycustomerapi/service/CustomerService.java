    package com.meghan.querycustomerapi.service;

    import com.meghan.querycustomerapi.entity.CustomerOrder;

    import java.util.concurrent.ExecutionException;

    public interface CustomerService {
        CustomerOrder getOrder(String customerId) throws ExecutionException, InterruptedException;
    }
