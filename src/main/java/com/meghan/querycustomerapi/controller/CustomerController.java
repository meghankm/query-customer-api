package com.meghan.querycustomerapi.controller;

import com.meghan.querycustomerapi.entity.CustomerOrder;
import com.meghan.querycustomerapi.service.CustomerService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping(value = "/cutomers")
@Api(value = "Query Customer", description = "Exposed Endpoint to Query Order placed by Customer by passing Customer ID", tags = "Query Customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/{customerId}/order")
    public CustomerOrder getOrder(@PathVariable String customerId) throws ExecutionException, InterruptedException {
        return customerService.getOrder(customerId);
    }
}
