package com.meghan.querycustomerapi.entity.repository;

import com.meghan.querycustomerapi.entity.CustomerOrder;
import org.springframework.data.repository.CrudRepository;

public interface CustomerOrderRepository extends CrudRepository<CustomerOrder, String> {
}

