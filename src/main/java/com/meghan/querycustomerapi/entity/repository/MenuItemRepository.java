package com.meghan.querycustomerapi.entity.repository;

import com.meghan.querycustomerapi.entity.MenuItem;
import org.springframework.data.repository.CrudRepository;

public interface MenuItemRepository extends CrudRepository<MenuItem, String> {
}

