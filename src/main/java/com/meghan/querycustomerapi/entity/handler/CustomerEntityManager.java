package com.meghan.querycustomerapi.entity.handler;

import com.meghan.querycustomerapi.axon.aggregate.CustomerAggregate;
import com.meghan.querycustomerapi.axon.event.OrderPlacedEvent;
import com.meghan.querycustomerapi.entity.CustomerOrder;
import com.meghan.querycustomerapi.entity.MenuItem;
import com.meghan.querycustomerapi.entity.repository.CustomerOrderRepository;
import com.meghan.querycustomerapi.entity.repository.MenuItemRepository;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.eventsourcing.EventSourcingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class CustomerEntityManager {

    @Autowired
    private CustomerOrderRepository customerOrderRepository;

    @Autowired
    private MenuItemRepository menuItemRepository;

    @Autowired
    @Qualifier("customerAggregateEventSourcingRepository")
    private EventSourcingRepository<CustomerAggregate> customerAggregateEventSourcingRepository;

    @EventSourcingHandler
    void on(OrderPlacedEvent event) {
        CustomerAggregate customerAggregate = customerAggregateEventSourcingRepository.load(event.getId())
                .getWrappedAggregate()
                .getAggregateRoot();

        CustomerOrder customer = new CustomerOrder();
        customer.setId(customerAggregate.getId());
        customer.setMerchantId(customerAggregate.getMerchantId());
        customer.setStatus(customerAggregate.getStatus());

        customerOrderRepository.save(customer);

        customerAggregate.getMenuItems().forEach(
                menuItem -> {
                    MenuItem item = new MenuItem();
                    item.setId(menuItem.getId());
                    item.setMenuItem(menuItem.getMenuItem());
                    item.setAmount(menuItem.getPrice().getAmount());
                    item.setCurrency(menuItem.getPrice().getCurrency());
                    item.setCategory(menuItem.getCategory());
                    menuItemRepository.save(item);
                }
        );
    }
}




