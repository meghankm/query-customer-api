package com.meghan.querycustomerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QueryCustomerApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(QueryCustomerApiApplication.class, args);
    }

}
