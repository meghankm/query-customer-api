package com.meghan.querycustomerapi.axon.event;

import com.meghan.querycustomerapi.model.MenuItem;

import java.util.List;

public class OrderPlacedEvent {

    private String id;
    private String merchantId;
    private List<MenuItem> menuItems;
    private String status;

    public OrderPlacedEvent() {
    }

    public OrderPlacedEvent(String id, String merchantId, List<MenuItem> menuItems, String status) {
        this.id = id;
        this.merchantId = merchantId;
        this.menuItems = menuItems;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "OrderPlacedEvent [ id=" + id + ", merchantId=" + merchantId + ", status=" + status + ", menuItems=" + menuItems + " ]";
    }
}
