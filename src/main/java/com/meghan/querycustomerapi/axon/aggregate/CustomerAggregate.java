package com.meghan.querycustomerapi.axon.aggregate;

import com.meghan.querycustomerapi.axon.command.PlaceOrderCommand;
import com.meghan.querycustomerapi.axon.event.OrderPlacedEvent;
import com.meghan.querycustomerapi.model.MenuItem;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

import java.util.List;

@Aggregate
public class CustomerAggregate {

    @AggregateIdentifier
    private String id;
    private String merchantId;
    private List<MenuItem> menuItems;
    private String status;

    CustomerAggregate() {
    }

    @CommandHandler
    public CustomerAggregate(PlaceOrderCommand command) {
        AggregateLifecycle.apply(new OrderPlacedEvent(command.getId(), command.getMerchantId(), command.getMenuItems(), "IN PROGRESS"));
    }

    @EventSourcingHandler
    protected void on(OrderPlacedEvent event) {
        this.id = event.getId();
        this.merchantId = event.getMerchantId();
        this.status = event.getStatus();
        this.menuItems = event.getMenuItems();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
